function sortByModel(inventory){
    if(inventory.length==0){
        return "Inventory is empty";
    }
    inventory.sort(function(a, b){
        if(a.car_model.toLowerCase() < b.car_model.toLowerCase()) { return -1; }
        if(a.car_model.toLowerCase() > b.car_model.toLowerCase()) { return 1; }
        return 0;
        })
    return inventory;
}
module.exports = { sortByModel };