const inputdata = require("./data");
const lib = require("../problem1");

const result = lib.getCarInfo(inputdata.inventory, 33);
console.log('Car 33 is a '+ result['car_year']+ ' ' + result['car_make']+ ' '+result['car_model']);