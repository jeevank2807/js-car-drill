const inputdata = require("./data");
const lib = require("../problem2");

const result = lib.getLastCar(inputdata.inventory);
console.log('Last car is a '+ result['car_make']+ ' '+result['car_model']);