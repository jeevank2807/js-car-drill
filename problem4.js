function getyearList(inventory){
    if(inventory.length==0){
        return "Inventory is empty";
    }
    let years=[];
    for(let i=0;i<inventory.length;i++){
        years.push(inventory[i]['car_year']);
    }
    return years;
}
module.exports = { getyearList };