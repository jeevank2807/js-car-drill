function getBMWAndAudi(inventory){
    if(inventory.length==0){
        return "Inventory is empty";
    }
    let BMWAndAudi=[];
    for(let i=0;i<inventory.length;i++){
        if(inventory[i]['car_make']=='BMW' || inventory[i]['car_make']=='Audi'){
            BMWAndAudi.push(inventory[i]);
        }
    }
    return BMWAndAudi;
}
module.exports = { getBMWAndAudi };