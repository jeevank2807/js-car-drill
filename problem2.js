function getLastCar(inventory){
    if(inventory.length==0){
        return "Inventory is empty";
    }
    return inventory.pop();
}


module.exports = { getLastCar };