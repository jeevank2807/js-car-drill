function olderThan2000(inventory){
    if(inventory.length==0){
        return "Inventory is empty";
    }
    let old=[];
    for(let i=0;i<inventory.length;i++){
        if(inventory[i]['car_year']<2000){
            old.push(inventory[i]['car_year']);
        }
    }
    return old;
}


module.exports = { olderThan2000 };