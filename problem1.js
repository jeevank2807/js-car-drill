function getCarInfo(inventory,id){
    if(inventory.length==0){
        return "Inventory is empty";
    }
    for (i=0;i<inventory.length;i++){
        if(inventory[i]['id']==id){
            return inventory[i];
        }
    }
}

module.exports = { getCarInfo };